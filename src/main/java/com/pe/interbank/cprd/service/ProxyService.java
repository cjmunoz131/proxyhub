package com.pe.interbank.cprd.service;
/**
 * Proxy Service Interface
 * @author fpinedaa
 *
 */
public interface ProxyService {

	void send(String request, String topic, boolean azFlag);
}
