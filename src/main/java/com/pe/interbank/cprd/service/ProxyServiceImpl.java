package com.pe.interbank.cprd.service;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.pe.interbank.cprd.integration.sb.ServiceBusProducer;

import pe.com.interbank.pys.trace.microservices.exceptions.MicroserviceException;
import pe.com.interbank.pys.trace.microservices.service.AbstractKafkaSenderService;
import pe.com.interbank.pys.trace.microservices.util.Constantes;
import pe.com.interbank.pys.trace.microservices.util.JsonUtil;

/**
 * Proxy Service
 * 
 * @author fpinedaa
 *
 */
@Service
public class ProxyServiceImpl implements ProxyService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProxyServiceImpl.class);

	private AbstractKafkaSenderService kafkaSenderService;
	
	@Autowired
	private ServiceBusProducer serviceBusProducer;
	
	@Inject
	public ProxyServiceImpl(AbstractKafkaSenderService kafkaSenderService) {
		this.kafkaSenderService = kafkaSenderService;
	}

	@Override
	@Async("ThreadPoolTaskExecutor")
	public void send(String request, String topic, boolean azFlag) {
		String messageId = JsonUtil.getCampoTrama(Constantes.MESSAGE_ID_MAP, request);
		try {
			if(!azFlag) {
				kafkaSenderService.enviarKafka(topic, request, messageId, -10L);
			}else {
				serviceBusProducer.sendMessageAsync(topic, request, messageId);
			}
		} catch (MicroserviceException e) {
			LOGGER.error("Error sending message to kafka, message: {}, topic: {}, {}", request, topic,
					Constantes.MESSAGE_ID_LOG + messageId, e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
