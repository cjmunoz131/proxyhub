package com.pe.interbank.cprd.integration.sb;

import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import javax.inject.Inject;

import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import com.microsoft.azure.servicebus.Message;
import com.microsoft.azure.servicebus.TopicClient;

@Component
public class ServiceBusProducer {
	
	
	
	private TopicClient topicClientSB;
	
	@Inject
	public ServiceBusProducer (TopicClient topicClient) {
		this.topicClientSB = topicClient;
	}
	

    public CompletableFuture<Void> sendMessageAsync(String topic, String request, String messageId) throws Exception {
    	
    	List<CompletableFuture> tasks = new ArrayList<CompletableFuture>();
    	Message message = new Message(request.getBytes(StandardCharsets.UTF_8));
    	message.setContentType("application/json");
        message.setLabel("notificacion");
        message.setMessageId(messageId);
        message.setTimeToLive(Duration.ofMinutes(2));
        tasks.add(
        		topicClientSB.sendAsync(message).thenRunAsync(() -> {
                    System.out.printf("\tMessage acknowledged: Id = %s\n", message.getMessageId());
                }));
        return CompletableFuture.allOf(tasks.toArray(new CompletableFuture<?>[tasks.size()]));
    }
}
