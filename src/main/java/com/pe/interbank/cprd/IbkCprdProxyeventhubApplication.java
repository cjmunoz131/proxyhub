package com.pe.interbank.cprd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IbkCprdProxyeventhubApplication {

	public static void main(String[] args) {
		SpringApplication.run(IbkCprdProxyeventhubApplication.class, args);
	}

}
