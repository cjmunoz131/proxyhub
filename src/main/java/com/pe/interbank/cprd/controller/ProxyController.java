package com.pe.interbank.cprd.controller;

import org.springframework.http.ResponseEntity;

/**
 * Proxy Controller Interface
 * @author fpinedaa
 *
 */
public interface ProxyController {
	
	ResponseEntity<?> read();

	ResponseEntity<?> sendRequestToTopic(String request, String topic);
	
	ResponseEntity<?> sendRequestToSBTopic(String request, String topic);
}
