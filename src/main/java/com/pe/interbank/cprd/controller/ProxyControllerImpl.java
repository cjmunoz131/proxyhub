package com.pe.interbank.cprd.controller;

import javax.inject.Inject;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pe.interbank.cprd.service.ProxyService;

import pe.com.interbank.pys.trace.microservices.util.PropertiesCache;
import pe.com.interbank.pys.trace.microservices.util.TraceConfigConstantes;

/**
 * Controller that let receive Rest Requests
 * 
 * @author fpinedaa
 *
 */
@RestController
@RequestMapping("/hub")
public class ProxyControllerImpl implements ProxyController {

	private ProxyService proxyService;
	
	@Inject
	public ProxyControllerImpl(ProxyService proxyService) {
		this.proxyService = proxyService;
	}

	@Override
	@GetMapping(produces = "application/json")
	public ResponseEntity<String> read() {
		return ResponseEntity.ok("Alive");
	}

	@PostMapping(produces = "application/json")
	public ResponseEntity<?> sendRequest(@RequestBody String request) {
		proxyService.send(request, PropertiesCache.getInstance().getProperty(TraceConfigConstantes.TOPICO_ASINCRONO) ,false);
		return ResponseEntity.accepted().build();
	}

	@Override
	@PostMapping(path = "/topic/{topic}")
	public ResponseEntity<?> sendRequestToTopic(@RequestBody String request,
			@PathVariable(required = true) String topic) {
		proxyService.send(request, topic, false);
		return ResponseEntity.accepted().build();
	}

	@Override
	@PostMapping(path = "/sb/topic/{topic}")
	public ResponseEntity<?> sendRequestToSBTopic(@RequestBody String request,
			@PathVariable(required = true) String topic) {
		// TODO Auto-generated method stub
		proxyService.send(request, topic, true);
		return ResponseEntity.accepted().build();
	}
	
}
