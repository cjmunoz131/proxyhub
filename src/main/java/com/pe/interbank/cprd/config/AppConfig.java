package com.pe.interbank.cprd.config;

import java.util.concurrent.Executor;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.microsoft.azure.servicebus.TopicClient;
import com.microsoft.azure.servicebus.primitives.ConnectionStringBuilder;
import com.microsoft.azure.servicebus.primitives.ServiceBusException;
import com.pe.interbank.cprd.util.ConfigConstantes;

import pe.com.interbank.pys.trace.microservices.azure.KafkaAuthenticationAzure;
import pe.com.interbank.pys.trace.microservices.service.AbstractKafkaSenderService;
import pe.com.interbank.pys.trace.microservices.service.KafkaSenderServiceAsyncImpl;
import pe.com.interbank.pys.trace.microservices.util.PropertiesCache;
/**
 * Configuration Class
 * @author fpinedaa
 *
 */
@Configuration
public class AppConfig {

	
	/**
	 * Bean para conexión con Kafka de Bluemix
	 @Bean
	public KafkaProducer<Integer, String> producerFactory() {
		return KafkaAuthentication.getProducer();
	}*/
	
	@Bean
	public KafkaProducer<Integer, String> producerFactory() {
		return KafkaAuthenticationAzure.getProducer();
	}
	
	@Bean
	@Qualifier("topicClientSB")
	public TopicClient topicClient() throws InterruptedException, ServiceBusException {
		TopicClient sendClient;
		ConnectionStringBuilder csb = new ConnectionStringBuilder(PropertiesCache.getInstance().getProperty(ConfigConstantes.CONNECTION_SB),PropertiesCache.getInstance().getProperty(ConfigConstantes.TOPIC_ASINCRONO_SB)); 
		sendClient = new TopicClient(csb);       
        return sendClient;
	}
	
	@Bean
	public AbstractKafkaSenderService kafkaAsyncService() {
		return new KafkaSenderServiceAsyncImpl();
	}
	
	@Bean(name = "ThreadPoolTaskExecutor")
	public Executor threadPoolTaskExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(5);
		executor.setMaxPoolSize(10);
		executor.setQueueCapacity(500);
		executor.setThreadNamePrefix("Async-");
		executor.initialize();
		return executor;
	}
}
