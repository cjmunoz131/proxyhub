package com.pe.interbank.cprd.util;

public class ConfigConstantes {

	
    public static final String TOPIC_ASINCRONO_SB = "servicebus.topic.asincrono";
    public static final String CONNECTION_SB = "connection.servicebus.athentication";
    public static final String NOTIFICATION_SUBSCRIPTION = "notificacion.subscripcion";
    
	protected ConfigConstantes() {
		throw new IllegalAccessError("Clase Constantes");
	}
}
