# ProxyEventHub
Expone API REST para enviar un mensaje al tópico asíncrono o a un tópico en específico:

> Post http://<iAddress>:<port>/hub
	With Body Json Request
	
> Post http://<iAddress>:<port>/hub/topic/{topicName}
	With Body Json Request

#### Temporal build artifact (.jar)

```sh
$ mvn clean install -o 
```

### Create ConfigMap
```sh
$ kubectl create configmap ibk-cprd-proxyeventhub-ms-properties --from-file=config-dev.properties
```

### Replace ConfigMap
```sh
$ kubectl create configmap ibk-cprd-proxyeventhub-ms-properties --dry-run --from-file=config-dev.properties -o yaml | kubectl replace -f -
```

##### Build Azure Dev with Tag

```sh
$ docker build -t="acreu2c003cudidev01.azurecr.io/ibk-cprd-proxyeventhub-ms:0.0.1" --build-arg artifact_id=ibk-cprd-proxyeventhub-ms --build-arg artifact_version=0.0.1 .
```
##### Push Azure Dev Container Registry
```sh
$ docker push acreu2c003cudidev01.azurecr.io/ibk-cprd-proxyeventhub-ms:0.0.1
```